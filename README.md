This project contains references examples of how to use the Boost.Test framework and the Google C++ Mocking 
framework.


To compile project you need to execute following commands:
    mkdir Debug
    cd Debug
    cmake .. # to configure project
    make # to compile code
    make test # to execute all tests

