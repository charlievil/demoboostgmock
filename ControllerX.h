#ifndef CONTROLLERX_H
#define	CONTROLLERX_H
#include "CommunicationX.h"


class ControllerX {
public:
    ControllerX(CommunicationX& fc) : fCommunication(fc) { }
    void doCalc(); 
    virtual ~ControllerX(){};

private:
    CommunicationX& fCommunication;
    
};


	


#endif	/* CONTROLLERX_H */

