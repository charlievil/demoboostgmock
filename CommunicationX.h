#ifndef COMMUNICATIONX_H
#define	COMMUNICATIONX_H
#include <string>
class CommunicationX {
public:
    CommunicationX()  { }
    virtual ~CommunicationX() { }
    virtual void send(const std::string& name, int value) = 0;
    virtual int receive(const std::string& name) = 0;
};

#endif	/* COMMUNICATIONX_H */

