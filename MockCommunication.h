
#ifndef MOCKCOMMUNICATION_H
#define	MOCKCOMMUNICATION_H
#include <gmock/gmock.h>
#include <string>
#include "CommunicationX.h"

class MockCommunication : public CommunicationX {
public:
    MockCommunication() { }
    virtual ~MockCommunication() { }
	MOCK_METHOD2(send, void(const std::string& name, int value));
	MOCK_METHOD1(receive, int(const std::string& name));
};



#endif	/* MOCKCOMMUNICATION_H */

