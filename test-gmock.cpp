/**
 * @file   test-mock.cpp
 * @author Alex Ott <alexott@gmail.com>
 *
 * @brief
 *
 *
 */

#include <gmock/gmock.h>
#include <string>
#define BOOST_TEST_MODULE Mock example
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include "ControllerX.h"
#include "CommunicationX.h"
#include "MockCommunication.h"



struct InitGMock {
	InitGMock() {
		::testing::GTEST_FLAG(throw_on_failure) = true;
		::testing::InitGoogleMock(&boost::unit_test::framework::master_test_suite().argc,
								  boost::unit_test::framework::master_test_suite().argv);
	}
	~InitGMock() { }
};
BOOST_GLOBAL_FIXTURE(InitGMock);

BOOST_AUTO_TEST_CASE(test_gmock) {
	using ::testing::Return;

	MockCommunication mockcommunication;
	EXPECT_CALL(mockcommunication, receive(std::string("test"))).Times(1).WillOnce(Return(101));
	EXPECT_CALL(mockcommunication, send(std::string("test2"),555));

	ControllerX controller(mockcommunication);
	controller.doCalc();
}

